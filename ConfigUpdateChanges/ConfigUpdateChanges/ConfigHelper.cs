﻿using System;
using System.IO;
using System.Xml;
using System.Linq;
using System.Reflection;
using System.Globalization;
using System.Collections.Generic;

namespace ConfigUpdateChanges
{
    public static class ConfigHelper
    {
        private static ConfigFile PegAppPath = new ConfigFile();
        static ConfigHelper()
        {

        }

        public static void UpdateConnString(Dictionary<string, string> MasterCourseDBConnString)
        {
            string MYPATH = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Pegasus");

            PegAppPath = GetTextData(MYPATH);
            MasterCourseDBConnString["Server"] = PegAppPath.Server;

            MasterCourseDBConnString["Database"] = PegAppPath.Database;
            MasterCourseDBConnString["UID"] = PegAppPath.UID;
            MasterCourseDBConnString["PWD"] = PegAppPath.PWD;
        }

        public static Dictionary<string, string> ToDictionaydata(string conn)
        {
            return conn.Split(';').Select(value => value.Split('=')).ToDictionary(pair => pair[0], pair => pair[1]);
        }

        public static string JoinArray(Dictionary<string, string> con)
        {
            return string.Join(";", con.Select(x => x.Key + "=" + x.Value).ToArray());
        }

        public static void UpdatePegasusconfigConnectionString(string connstringPath)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(connstringPath);
            XmlElement Root = doc.DocumentElement;
            XmlNodeList appNode = Root.SelectNodes("add");
            foreach (XmlElement node in appNode)
            {
                if (node != null)
                {
                    try
                    {
                        string key1 = node.Attributes.GetNamedItem("connectionString").Value;
                        Dictionary<string, string> ConnString = key1.ToString().Split(';').Select(value => value.Split('=')).ToDictionary(pair => pair[0], pair => pair[1]);
                        UpdateConnString(ConnString);
                        node.Attributes.GetNamedItem("connectionString").Value = string.Join(";", ConnString.Select(x => x.Key + "=" + x.Value).ToArray());
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("\nException occurred : " + ex.Message);
                    }
                }
            }
            doc.Save(connstringPath);
        }

        public static void UpdateWebconfig(string confiFile, ConfigFile PegAppPath)
        {
            string configFile = @confiFile;

            try
            {
                // Load the configuration file
                XmlDocument doc = new XmlDocument();
                doc.Load(configFile);

                // Find the appSettings section
                XmlNode appSettingsNode = doc.SelectSingleNode("/configuration/appSettings");

                if (appSettingsNode != null)
                {
                    // Find the specific key elements
                    XmlNode gburl = appSettingsNode.SelectSingleNode("add[@key='GradeBookUrl']");
                    XmlNode contenturl = appSettingsNode.SelectSingleNode("add[@key='ContentServiceURI']");
                    XmlNode UsersEndPoint = appSettingsNode.SelectSingleNode("add[@key='UsersEndPoint']");

                    if (gburl != null)
                    {
                        // Update the value of GradeBookUrl
                        gburl.Attributes["value"].Value = PegAppPath.PegasusNextGenGradeBookApiPath;
                    }

                    if (contenturl != null)
                    {
                        // Update the value of ContentServiceURI
                        contenturl.Attributes["value"].Value = PegAppPath.NotificationContentApiPath;
                    }

                    if (UsersEndPoint != null)
                    {
                        // Update the value of UsersEndPoint
                        UsersEndPoint.Attributes["value"].Value = PegAppPath.NextGenAuthProviderApiPath;
                    }

                    // Save the changes
                    doc.Save(configFile);
                }
                else
                {
                    Console.WriteLine("appSettings section not found in the configuration.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred: {ex.Message}");
            }
        }

        public static ConfigFile GetTextData(string MYPATH)
        {
            string jsonfile = MYPATH + MappingHelper.PegasusEntryPath;

            //string pegFolderpath = "";

            var configJson = File.ReadAllText(jsonfile);

            PegasusConfig config = JsonHelper.JsonDeserializer<PegasusConfig>(configJson);

            PegAppPath = config.Pegasus;

            return PegAppPath;

//            string[] pegFolderpath = new string[] { };

//            if (File.Exists(TextFile))
//            {
//                pegFolderpath = (File.ReadAllLines(MYPATH + MappingHelper.PegasusEntryPath));
//                var dict = pegFolderpath.Where(x => x.Contains("=")).ToList().Select(line => line.Split('=')).ToDictionary(split => split[0], split => split[1]);
//                PegAppPath.PegAppPath = dict["PegAppPath"];
//                PegAppPath.PegNextGenAppPath = dict["PegNextGenAppPath"];

//                if (dict.ContainsKey("Server") && string.IsNullOrEmpty(PegAppPath.Server) && !string.IsNullOrEmpty(ServerName.DbServerName))
//                {
//                    PegAppPath.Server = dict["Server"] != "" ? dict["Server"] : ServerName.DbServerName;
//                }
//                if (dict.ContainsKey("Database"))
//                {
//                    PegAppPath.Database = dict["Database"];
//                }
//                if (dict.ContainsKey("UID"))
//                {
//                    PegAppPath.UID = dict["UID"];
//                }
//                if (dict.ContainsKey("PWD"))
//                {
//                    PegAppPath.PWD = dict["PWD"];
//                }
//                if (dict.ContainsKey("Ngc"))
//                {
//                    PegAppPath.Ngc = dict["Ngc"];
//                }
//                if (dict.ContainsKey("redisserver"))
//                {
//                    PegAppPath.Redisserver = dict["redisserver"];
//                }
//                if (dict.ContainsKey("PegasusNotificationsAppPath"))
//                    PegAppPath.PegasusNotificationsAppPath = dict["PegasusNotificationsAppPath"];

//                if (dict.ContainsKey("PegasusNotificationsApiPath"))
//                    PegAppPath.PegasusNotificationsApiPath = dict["PegasusNotificationsApiPath"];

//                if (dict.ContainsKey("NextGenAuthProviderApiPath"))
//                    PegAppPath.NextGenAuthProviderApiPath = dict["NextGenAuthProviderApiPath"];

//                if (dict.ContainsKey("EnableNextGenViewSubmission"))
//                    PegAppPath.EnableNextGenViewSubmission = dict["EnableNextGenViewSubmission"];

//                if (dict.ContainsKey("NextGenGatewayApiPath"))
//                    PegAppPath.NextGenGatewayApiPath = dict["NextGenGatewayApiPath"];

//                if (dict.ContainsKey("NextGenViewSubmissionAppPath"))
//                    PegAppPath.NextGenViewSubmissionAppPath = dict["NextGenViewSubmissionAppPath"];

//                if (dict.ContainsKey("PegasusNextGenReportAppPath"))
//                    PegAppPath.PegasusNextGenReportAppPath = dict["PegasusNextGenReportAppPath"];

//                if (dict.ContainsKey("PegasusNextGenReportApiPath"))
//                    PegAppPath.PegasusNextGenReportApiPath = dict["PegasusNextGenReportApiPath"];

//                if (dict.ContainsKey("PegasusNextGenGradeBookApiPath"))
//                    PegAppPath.PegasusNextGenGradeBookApiPath = dict["PegasusNextGenGradeBookApiPath"];

//                if (dict.ContainsKey("PegasusNextGenGradebookAppPath"))
//                    PegAppPath.PegasusNextGenGradebookAppPath = dict["PegasusNextGenGradebookAppPath"];

//                if (dict.ContainsKey("PegasusNextGenContentApiPath"))
//                    PegAppPath.PegasusNextGenContentApiPath = dict["PegasusNextGenContentApiPath"];

//                if (dict.ContainsKey("NextGenAdminApiPath"))
//                    PegAppPath.NextGenAdminApiPath = dict["NextGenAdminApiPath"];

//                if (dict.ContainsKey("NextGenIntegrationApiPath"))
//                    PegAppPath.NextGenIntegrationApiPath = dict["NextGenIntegrationApiPath"];
//                if (dict.ContainsKey("MsBuildPath"))
//                    PegAppPath.MsBuildPath = dict["MsBuildPath"];
//                if (!dict.ContainsKey("Database") || !dict.ContainsKey("UID") || !dict.ContainsKey("PWD") || !dict.ContainsKey("ngc"))
//                {
//                    Console.WriteLine("\nNote : Please update your DB details to this path " + MYPATH + MappingHelper.PegasusEntryPath + " \n" +

//@"----------------------------- " + " \n" +
//"Server" + PegAppPath.Server + " \n" +
//@"Database=Pegasus517
//UID=sa
//PWD=Sa123456
//ngc=ngcoreservices 
//----------------------------");
//                    Console.WriteLine("\nWe have updated above DB details by default. Please update the value to PegasusFolderEntryPath.txt, If required. Press any Key to Exit");
//                    Console.ReadKey();
//                    Environment.Exit(0);
//                }
//            }
//            return PegAppPath;
        }

        public static ConsoleKeyInfo ValidateUserInput()
        {
            ConsoleKeyInfo keyInfo;
            do
            {
                keyInfo = Console.ReadKey(true); // Read key without displaying it
            } while (keyInfo.Key != ConsoleKey.Y && keyInfo.Key != ConsoleKey.N);
            return keyInfo;
        }

        public static void UpdateCacheServer(XmlDocument cachexml, string cacheconfig,string AppName)
        {
            Console.Write("\nDo You Want to update " + AppName + " Cache Server " + " ? Y/N: ");
            ConsoleKeyInfo abc = ConfigHelper.ValidateUserInput();
            if (abc.Key == ConsoleKey.Y)
            {
                // Get the root element (CacheConfiguration)
                XmlElement rootElement = cachexml.DocumentElement;

                // Check if the CacheProvider attribute is "RedisCache"
                if (rootElement.GetAttribute("CacheProvider") == "RedisCache")
                {
                    // Update the RedisCacheServer attribute value
                    string newRedisCacheServer = PegAppPath.Redisserver;
                    rootElement.SetAttribute("RedisCacheServer", newRedisCacheServer);
                }

                // Convert the modified XML back to a string
                //string modifiedXml = cachexml.OuterXml;
                cachexml.Save(cacheconfig);
                Console.WriteLine("\n Cache Server Updated for " + AppName);
            }
        }

        public static string JsonSerialize(Dictionary<string,object> configs,string PegAppPath)
        {
            string assmeblyname = PegAppPath + @"\ExternalAssemblies\JWT";
            string jsondata = "";

            AppDomain myDomain = AppDomain.CurrentDomain;
            var serviceAgentAssembly = System.Reflection.Assembly.LoadFrom(string.Format(CultureInfo.InvariantCulture, @"{0}\{1}", assmeblyname, "Newtonsoft.Json.dll"));

            var method_serialize = serviceAgentAssembly.GetTypes().Where(x => x.Name == "JsonConvert").Select(y => y.GetRuntimeMethods().Where(z => z.Name == "SerializeObject" && z.ReturnType.Name == "String" && z.GetParameters().Length == 2).ToList()).FirstOrDefault();
            jsondata = method_serialize.Select(x => x).Where(y => y.GetParameters().Any(z => z.ParameterType.FullName == "Newtonsoft.Json.Formatting")).FirstOrDefault().Invoke(jsondata, new object[] { configs, 1 }).ToString();
            return jsondata;
        }
    }
}
