﻿using System;
using System.Collections.Generic;
using static ConfigUpdateChanges.Enumeration;

namespace ConfigUpdateChanges
{
    class MappingHelper
    {
        public const string VS = "viewsubmission";
        public const string ngAdmin = "coreservicesA";
        public const string ngContent = "coreservicesC";
        public const string ngIntegration = "coreservicesI";
        public const string ViewSubmission = "ViewSubmission";
        public const string TV = "TV";
        public const string todaysview = "todaysview";
        public const string AuthProvider = "AuthProvider";
        public const string CoreServiceProvider = "CoreServiceProvider";
        public const string BatchFile = "ExecuteMultipleSQLFileWithoutCredentials.bat";
        public const string PegasusEntryPath = "\\PegasusFolderEntryPath.json";
        public const string BackwardSlash = "\\";
        public const string PegSPInstaller = "Installer.exe";
        public const string Schema_Upgrade = "Dev_Pegasus517_Schema_Upgrade.bat";
        public const string Dev_Pegasus517_Upgrade = "Dev_Pegasus517_Upgrade.bat";
        public const string services = "\\src\\Services\\";
        public const string API = ".API";
        public const string csProject = ".csproj";
        public const string ConnString = "connectionstring.json";
        public const string AppSettings = "appsettings.json";
        public const string NotificationConnPath = "\\todaysview\\src\\Services\\Notifications\\Pegasus.Config\\pegasus.connectionStrings.config";
        public const string NotificationRootPath = "\\todaysview\\src\\Services\\Notifications\\Pegasus.Notifications\\Notifications.Api\\";

        public static Dictionary<string, string> AppcsProjectName = new Dictionary<string, string>() { 
            {"Admin", "Admin\\Admin.API"},
            {"Content", "Content\\Content.API"},
            {"Integration", "Integration\\Integration.API"},
            {"gradebook", "GradeBook\\GradeBook.API"},
            {"reports", "Reports\\Reports.API"},
            {"Notifications", "Notifications\\Pegasus.Notifications\\Notifications.Api"},
            {"ViewSubmission", "Assessment.ViewSubmission\\ViewSubmission.API"},
        };

        public static Dictionary<Int16, string> CachingNames = new Dictionary<short, string>()
        {
            { (Int16)Caching.InProc, "//sessionState[@mode='InProc']" },
            { (Int16)Caching.RedisCache, "//sessionState[@customProvider='MySessionStateStore']" },
            { (Int16)Caching.MicrosoftCaching, "//sessionState[@customProvider='SessionStoreProvider']" },
            { (Int16)Webconfigkey.httpCookies, "//httpCookies[@httpOnlyCookies='true']" },
            { (Int16)Webconfigkey.ConetntSecurityPolicy, "//add[@name='Content-Security-Policy']" },
            { (Int16)Webconfigkey.SessionWrapperModule, "//add[@name='SessionWrapperModule']" },
            { (Int16)Con.ConnectionString, "//add[@name='MySessionStateStore']" }
        };

        public static Dictionary<Int16, string> sessionState = new Dictionary<short, string>()
        {
            { (Int16)Caching.InProc, "tcpip=127.0.0.1:42424" },
            { (Int16)Caching.RedisCache, "Microsoft.Web.Redis.RedisSessionStateProvider" },
            { (Int16)Caching.MicrosoftCaching, "Microsoft.ApplicationServer.Caching.DataCacheSessionStoreProvider,Microsoft.ApplicationServer.Caching.Client, Version=1.0.0.0,Culture=neutral, PublicKeyToken=31bf3856ad364e35" },
        };

        public static Dictionary<Int16, string> NextGenSPAppPath = new Dictionary<short, string>()
        {
            { (Int16)PegNextGenSPPath.SP_Path, "\\src\\Database\\Code\\StoredProcedures\\" }
        };

        public static Dictionary<Int16, string> PegasusSPAppPath = new Dictionary<short, string>()
        {
            { (Int16)PegasusSPPath.PegSPPath, "\\PegasusDatabase\\DBScripts\\util\\DB_Installer\\" }
        };
    }
}
