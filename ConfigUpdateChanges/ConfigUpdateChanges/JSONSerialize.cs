﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Runtime.Serialization.Json;

namespace ConfigUpdateChanges
{
    public static class JSONSerialize
    {
        public static class JSONSerializer<TType> where TType : class
        {
            /// <summary>
            /// Serializes an object to JSON
            /// </summary>
            public static string Serialize(TType instance)
            {
                var serializer = new DataContractJsonSerializer(typeof(TType));
                using (var stream = new MemoryStream())
                {
                    serializer.WriteObject(stream, instance);
                    string json = Encoding.UTF8.GetString(stream.ToArray());

                    return FormatJson(json);
                }
            }

            /// <summary>
            /// Formatting the Json object
            /// </summary>
            /// <param name="json"></param>
            /// <param name="indent"></param>
            /// <param name="times"></param>
            /// <returns></returns>
            public static string FormatJson(string json, char indent = ' ', int times = 2)
            {
                var indentation = 0;
                var quoteCount = 0;
                var indentString = string.Concat(Enumerable.Repeat(indent, times));
                var result = "";
                foreach (var ch in json)
                {
                    switch (ch)
                    {
                        case '"':
                            quoteCount++;
                            result += ch;
                            break;
                        case ',':
                            if (quoteCount % 2 == 0)
                            {
                                result += ch + Environment.NewLine +
                                string.Concat(Enumerable.Repeat(indentString, indentation));
                            }
                            break;
                        case '{':
                        case '[':
                            var openFormatted = string.Concat(Enumerable.Repeat(indentString, ++indentation))
                            + indent;
                            result += string.Format("{0}{1}{2}", ch, Environment.NewLine, openFormatted);
                            break;
                        case '}':
                        case ']':
                            var closeFormatted = string.Concat(Enumerable.Repeat(indentString, --indentation));
                            result += string.Format("{0}{1}{2}", Environment.NewLine, closeFormatted, ch);
                            break;
                        default:
                            result += ch;
                            break;
                    }
                }
                return result;
            }
            /// <summary>
            /// DeSerializes an object from JSON
            /// </summary>
            public static TType DeSerialize(string json)
            {
                // Remove single-line comments (// ...)
                json = Regex.Replace(json, @"\/\/.*", "");

                // Remove multi-line comments (/* ... */)
                json = Regex.Replace(json, @"/\*.*?\*/", "", RegexOptions.Singleline);

                using (var stream = new MemoryStream(Encoding.Default.GetBytes(json)))
                {
                    var serializer = new DataContractJsonSerializer(typeof(TType));
                    return serializer.ReadObject(stream) as TType;
                }
            }
        }
        public class OTLJSON<T> where T : class
        {
            /// <summary>
            /// Serializes an object to JSON
            /// Usage: string serialized = OTLJSON&lt;MusicInfo&gt;.Serialize(musicInfo);
            /// </summary>
            /// <param name="instance"></param>
            /// <returns></returns>
            public static string Serialize(T instance)
            {
                var serializer = new DataContractJsonSerializer(typeof(T));
                using (var stream = new MemoryStream())
                {
                    serializer.WriteObject(stream, instance);
                    return Encoding.Default.GetString(stream.ToArray());
                }
            }

            /// <summary>
            /// DeSerializes an object from JSON
            /// Usage:  MusicInfo deserialized = OTLJSON&lt;MusicInfo&gt;.Deserialize(json);
            /// </summary>
            /// <param name="json"></param>
            /// <returns></returns>
            public static T Deserialize(string json)
            {
                if (string.IsNullOrEmpty(json))
                    throw new Exception("Json can't empty");
                else
                    try
                    {
                        using (var stream = new MemoryStream(Encoding.Default.GetBytes(json)))
                        {

                            var serializer = new DataContractJsonSerializer(typeof(T));
                            return serializer.ReadObject(stream) as T;
                        }
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Json can't convert to Object because it isn't correct format." + e.Message);
                    }
            }


        }

    }
    public static class JsonHelper
    {
        public static R JConvert<P, R>(this P t)
        {
            if (typeof(P) == typeof(string))
            {
                var return1 = (R)(JsonDeserializer<R>(t as string));
                return return1;
            }
            else
            {
                var return2 = (JsonSerializer<P>(t));
                R result = (R)Convert.ChangeType(return2, typeof(R));
                return result;
            }
        }

        public static String JsonSerializer<T>(T t)
        {
            var stream1 = new MemoryStream();
            var ser = new DataContractJsonSerializer(typeof(T));
            ser.WriteObject(stream1, t);
            stream1.Position = 0;
            var sr = new StreamReader(stream1);
            return (sr.ReadToEnd());
        }

        public static T JsonDeserializer<T>(string jsonString)
        {
            T deserializedUser = default(T);
            var ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            var ser = new DataContractJsonSerializer(typeof(T));
            deserializedUser = (T)ser.ReadObject(ms);// as T;
            ms.Close();
            return deserializedUser;
        }
    }
}
