﻿using System;

namespace ConfigUpdateChanges
{
    public class Program
    {
      [STAThread]
      public static void Main(string[] args)
        {
            try
            {
                ConfigChanges cnfc = new ConfigChanges();
                cnfc.ChangeConfigfile();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occurred : " + ex.Message.ToString());
            }
        }
    }
}
