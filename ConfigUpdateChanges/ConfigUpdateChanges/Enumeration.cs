﻿namespace ConfigUpdateChanges
{
    class Enumeration
    {
        public enum Caching
        {
            InProc = 1,
            RedisCache = 2,
            MicrosoftCaching = 3
        }

        public enum Webconfigkey
        {
            httpCookies = 5,
            SessionWrapperModule = 6,
            ConetntSecurityPolicy = 7
        }

        public enum Con
        {
            ConnectionString = 4
        }

        public enum PegNextGenAppPath
        {
            coreservicesA = 1,
            coreservicesC = 2,
            gradebook = 3,
            reports = 4,
            todaysview = 5,
            viewsubmission = 6,
            coreservicesI = 7,
        }

        public enum NextGenAppPath
        {
            ViewSubmissions = 1,
            Notifications = 2,
            Admin =3,
            Content = 4,
            Integration = 5,
        }

        public enum PegNextGenSPPath
        {
            SP_Path = 1
        }

        public enum PegasusSPPath
        {
            PegSPPath = 1
        }

    }
}
