﻿using System;
using System.IO;
using System.Xml;
using System.Linq;
using System.Reflection;
using System.Diagnostics;
using System.Windows.Forms;
using System.Globalization;
using System.Collections.Generic;
using static ConfigUpdateChanges.Enumeration;
using static ConfigUpdateChanges.JSONSerialize;

namespace ConfigUpdateChanges
{
    public static class PegasusNextGen
    {
        static ConfigFile PegAppPath = new ConfigFile();
        static string MYPATH = "";
        static PegasusNextGen()
        {
            MYPATH = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Pegasus");

            PegAppPath = ConfigHelper.GetTextData(MYPATH);
        }

        public static void UpdateNextgenConnectionString()
        {
            Dictionary<string, string> NextGenrepos = GetAllNextGenAppPath();
            if (NextGenrepos.ContainsKey("CoreServiceProvider"))
            { // check key before removing it
                NextGenrepos.Remove("CoreServiceProvider");
                NextGenrepos.Remove("Integration.API");
            }
            try
            {
                foreach (var nxgenapppath in NextGenrepos)
                {
                    string nxgenconnectionstringpath = "";
                    if (nxgenapppath.Key.ToString() == "Notifications.API")
                    {
                        nxgenconnectionstringpath = PegAppPath.PegNextGenAppPath + MappingHelper.NotificationConnPath;
                        ConfigHelper.UpdatePegasusconfigConnectionString(nxgenconnectionstringpath);
                        Console.WriteLine("\n Connectionstring Updated for " + nxgenapppath.Key.ToString());
                    }
                    else
                    {
                        nxgenconnectionstringpath = nxgenapppath.Value + MappingHelper.BackwardSlash + MappingHelper.ConnString;
                        var configJson = File.ReadAllText(nxgenconnectionstringpath);

                        var config = JSONSerializer<connectionstring>.DeSerialize(configJson);

                        Dictionary<string, string> MasterCourseDBConnString = ConfigHelper.ToDictionaydata(config.PegasusConnectionStrings.MasterCourseDBConnString.ToString());
                        ConfigHelper.UpdateConnString(MasterCourseDBConnString);
                        config.PegasusConnectionStrings.MasterCourseDBConnString = ConfigHelper.JoinArray(MasterCourseDBConnString);

                        Dictionary<string, string> PegasusAdminConnection = ConfigHelper.ToDictionaydata(config.PegasusConnectionStrings.PegasusAdminConnection.ToString());
                        ConfigHelper.UpdateConnString(PegasusAdminConnection);
                        config.PegasusConnectionStrings.PegasusAdminConnection = ConfigHelper.JoinArray(PegasusAdminConnection);

                        Dictionary<string, string> PegasusDBConnection1 = ConfigHelper.ToDictionaydata(config.PegasusConnectionStrings.PegasusDBConnection1.ToString());
                        ConfigHelper.UpdateConnString(PegasusDBConnection1);
                        config.PegasusConnectionStrings.PegasusDBConnection1 = ConfigHelper.JoinArray(PegasusDBConnection1);

                        if (!string.IsNullOrEmpty(config.PegasusConnectionStrings.ReportingConnection))
                        {
                            Dictionary<string, string> ReportingConnection = ConfigHelper.ToDictionaydata(config.PegasusConnectionStrings.ReportingConnection.ToString());
                            ConfigHelper.UpdateConnString(ReportingConnection);
                            config.PegasusConnectionStrings.ReportingConnection = ConfigHelper.JoinArray(ReportingConnection);
                        }

                        // This will produce a copy of the instance you created earlier
                        var updatedConfigJson = JSONSerializer<connectionstring>.Serialize(config);
                        File.WriteAllText(nxgenconnectionstringpath, updatedConfigJson);
                        Console.WriteLine("\n Connectionstring Updated for " + nxgenapppath.Key.ToString());
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("\nError occured" + ex.Message);
            }

        }

        public static void UpdateNextgenAppsettings()
        {
            Dictionary<string, string> NextGenrepos = GetAllNextGenAppPath();
            try
            {
                foreach (var nxgenapppath in NextGenrepos)
                {
                    string appsettingsjsonpath = "";
                    if (nxgenapppath.Key.ToString() == "Notifications.API")
                    {
                        string nxgenconnectionstringpath = PegAppPath.PegNextGenAppPath + MappingHelper.NotificationRootPath + "Web.config";
                        ConfigHelper.UpdateWebconfig(nxgenconnectionstringpath, PegAppPath);
                        Console.WriteLine("\n Web.config Updated for " + nxgenapppath.Key.ToString());
                    }
                    else
                    {
                        appsettingsjsonpath = nxgenapppath.Value + MappingHelper.BackwardSlash + MappingHelper.AppSettings;
                        var configJson = File.ReadAllText(appsettingsjsonpath);

                        var CutomconfigJson = File.ReadAllText(MYPATH + MappingHelper.PegasusEntryPath);

                        string assmeblyname = PegAppPath.PegAppPath + @"\ExternalAssemblies\JWT";
                        string jsondata = "";

                        AppDomain myDomain = AppDomain.CurrentDomain;
                        var serviceAgentAssembly = System.Reflection.Assembly.LoadFrom(string.Format(CultureInfo.InvariantCulture, @"{0}\{1}", assmeblyname, "Newtonsoft.Json.dll"));
                        dynamic securityvalue = null;
                        dynamic dny;
                        var method_desrialize = serviceAgentAssembly.GetTypes().Where(x => x.Name == "JsonConvert").Select(y => y.GetRuntimeMethods().Where(z => z.Name == "DeserializeObject" && z.ReturnType.Name == "Object" && z.GetParameters().Length == 1)).FirstOrDefault();
                        dny = method_desrialize.FirstOrDefault().Invoke(securityvalue, new object[] { configJson });

                        dynamic dny1;
                        var method_desrialize1 = serviceAgentAssembly.GetTypes().Where(x => x.Name == "JsonConvert").Select(y => y.GetRuntimeMethods().Where(z => z.Name == "DeserializeObject" && z.ReturnType.Name == "Object" && z.GetParameters().Length == 1)).FirstOrDefault();
                        dny1 = method_desrialize1.FirstOrDefault().Invoke(securityvalue, new object[] { CutomconfigJson });
                        var data = dny1[nxgenapppath.Key];
                        foreach (var configkey in data)
                        {
                            dny["AppSettings"][configkey.Name] = configkey.Value.ToString();
                        }
                        var method_serialize = serviceAgentAssembly.GetTypes().Where(x => x.Name == "JsonConvert").Select(y => y.GetRuntimeMethods().Where(z => z.Name == "SerializeObject" && z.ReturnType.Name == "String" && z.GetParameters().Length == 2).ToList()).FirstOrDefault();
                        jsondata = method_serialize.Select(x => x).Where(y => y.GetParameters().Any(z => z.ParameterType.FullName == "Newtonsoft.Json.Formatting")).FirstOrDefault().Invoke(jsondata, new object[] { dny, 1 }).ToString();

                        File.WriteAllText(appsettingsjsonpath, jsondata);
                        Console.WriteLine("\n appsettingsjson Updated for " + nxgenapppath.Key);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("\nError occured inside UpdateNextgenAppsettings " + ex.Message);
            }

        }

        public static void BuildApplication()
        {
            Dictionary<string, string> NextGenrepos = GetAllNextGenAppPath();
            foreach (var reponame in NextGenrepos)
            {
                if (Directory.Exists(reponame.Value.ToString()))
                {
                    Console.Write("\nDo You Want to Build this repo " + reponame.Key + " ? Y/N: ");
                    ConsoleKeyInfo abc = ConfigHelper.ValidateUserInput();
                    if (abc.Key == ConsoleKey.Y)
                    {
                        Process proc = new Process();
                        proc.StartInfo = new ProcessStartInfo(@PegAppPath.MsBuildPath);
                        string projectPath = @reponame.Value;
                        proc.StartInfo.Arguments = string.Format(@"{0}\{1}", projectPath, reponame.Key + MappingHelper.csProject + " /p:Configuration=Debug /t:Clean;Rebuild");
                        proc.Start();
                        proc.WaitForExit();

                        //string devEnv = @"C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\Common7\IDE\devenv.exe";
                        //string msBuild = @"C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\MSBuild\Current\Bin\MSBuild.exe";
                        //string compileProject = string.Format("{0}\" \"{1}\" -t:rebuild", msBuild, projectPath);
                        ////string compileProject = string.Format("/c \"{0}\" \"{1}\" /build", devEnv, projectPath);
                        //Process.Start(compileProject);
                        Console.WriteLine("\nBuild Succeeded for " + reponame.Key);
                    }
                }
                else
                {
                    Console.WriteLine("\nRepo path does not exists: " + reponame.Value.ToString());
                }
            }
        }

        public static void UpdateNextGenCacheServer()
        {
            Dictionary<string, string> NextGenrepos = GetAllNextGenAppPath();
            if (NextGenrepos.ContainsKey("CoreServiceProvider"))
            { // check key before removing it
                NextGenrepos.Remove("CoreServiceProvider");
            }
            foreach (var reponame in NextGenrepos)
            {
                if (Directory.Exists(reponame.Value.ToString()))
                {
                    if (reponame.Key.Contains("Notifications.API"))
                    {
                        string CacheConfigpath = PegAppPath.PegNextGenAppPath + MappingHelper.NotificationRootPath + "Cache.config";

                        XmlDocument cachexml = new XmlDocument();

                        cachexml.Load(CacheConfigpath);

                        ConfigHelper.UpdateCacheServer(cachexml, CacheConfigpath, "Notification API");
                    }
                    else
                    { // check key before removing it
                        Console.Write("\nDo You Want to Update Cache Server " + reponame.Key + " ? Y/N: ");
                        ConsoleKeyInfo cache = ConfigHelper.ValidateUserInput();
                        if (cache.Key == ConsoleKey.Y)
                        {
                            UpdateCachesettingsjson(reponame.Value);
                        }
                    }
                }
                else
                {
                    Console.WriteLine("\nRepo path does not exists: " + reponame.Value.ToString());
                }
            }
        }

        public static Dictionary<string, string> GetAllNextGenSPAppPath(string pegNextGenAppPath)
        {
            Dictionary<string, string> spAppPath = new Dictionary<string, string>();

            var PegNxGenAppPath = Enum.GetValues(typeof(PegNextGenAppPath)).Cast<PegNextGenAppPath>().ToList();
            PegNxGenAppPath.Remove(PegNextGenAppPath.coreservicesI);

            string AppName = "";

            foreach (var DevBranchName in PegNxGenAppPath)
            {
                string branchname = DevBranchName.ToString();
                AppName = branchname;
                if (DevBranchName.ToString().Contains(MappingHelper.ngAdmin))
                {
                    if (!string.IsNullOrEmpty(PegAppPath.Ngc))
                    {
                        branchname = PegAppPath.Ngc;
                    }
                    AppName = Enum.GetName(typeof(NextGenAppPath), NextGenAppPath.Admin);
                }
                if (DevBranchName.ToString().Contains(MappingHelper.ngContent))
                {
                    if (!string.IsNullOrEmpty(PegAppPath.Ngc))
                    {
                        branchname = PegAppPath.Ngc;
                    }
                    AppName = Enum.GetName(typeof(NextGenAppPath), NextGenAppPath.Content);
                }
                if (AppName.Contains(MappingHelper.VS))
                {
                    AppName = Enum.GetName(typeof(NextGenAppPath), NextGenAppPath.ViewSubmissions);
                }
                if (AppName.Contains(MappingHelper.todaysview))
                {
                    AppName = Enum.GetName(typeof(NextGenAppPath), NextGenAppPath.Notifications);
                }
                string spAppPathname = pegNextGenAppPath + MappingHelper.BackwardSlash + branchname + MappingHelper.NextGenSPAppPath[Convert.ToInt16((PegNextGenSPPath.SP_Path))] + AppName;
                spAppPath.Add(AppName, spAppPathname);
            }
            return spAppPath;
        }

        public static void updateappsettingsjson()
        {
            string appsettingsjsonpath = PegAppPath.PegNextGenAppPath + @"\gradebook\src\Services\GradeBook\GradeBook.API\appsettings.json";
            var configJson = File.ReadAllText(appsettingsjsonpath);

            string assmeblyname = PegAppPath.PegAppPath + @"\ExternalAssemblies\JWT";

            AppDomain myDomain = AppDomain.CurrentDomain;

            var serviceAgentAssembly = System.Reflection.Assembly.LoadFrom(string.Format(CultureInfo.InvariantCulture, @"{0}\{1}", assmeblyname, "Newtonsoft.Json.dll"));
            dynamic securityvalue = null;
            dynamic dny;
            string jsondata = "";
            var method_desrialize = serviceAgentAssembly.GetTypes().Where(x => x.Name == "JsonConvert").Select(y => y.GetRuntimeMethods().Where(z => z.Name == "DeserializeObject" && z.ReturnType.Name == "Object" && z.GetParameters().Length == 1)).FirstOrDefault();
            dny = method_desrialize.FirstOrDefault().Invoke(securityvalue, new object[] { configJson });
            dny["AppSettings"]["IsRabbitmqEnabled"] = false;
            dny["AppSettings"]["IsAutobahnEnabled"] = false;

            var method_serialize = serviceAgentAssembly.GetTypes().Where(x => x.Name == "JsonConvert").Select(y => y.GetRuntimeMethods().Where(z => z.Name == "SerializeObject" && z.ReturnType.Name == "String" && z.GetParameters().Length == 2).ToList()).FirstOrDefault();
            jsondata = method_serialize.Select(x => x).Where(y => y.GetParameters().Any(z => z.ParameterType.FullName == "Newtonsoft.Json.Formatting")).FirstOrDefault().Invoke(jsondata, new object[] { dny, 1 }).ToString();

            File.WriteAllText(appsettingsjsonpath, jsondata);
            Console.WriteLine("\n Gb appsettingsjson Updated");
        }

        public static void UpdateCachesettingsjson(string apppath)
        {
            string appsettingsjsonpath = apppath + @"\CacheSettings.json";
            var configJson = File.ReadAllText(appsettingsjsonpath);

            string assmeblyname = PegAppPath.PegAppPath + @"\ExternalAssemblies\JWT";

            AppDomain myDomain = AppDomain.CurrentDomain;

            var serviceAgentAssembly = System.Reflection.Assembly.LoadFrom(string.Format(CultureInfo.InvariantCulture, @"{0}\{1}", assmeblyname, "Newtonsoft.Json.dll"));
            dynamic securityvalue = null;
            dynamic dny;
            string jsondata = "";
            var method_desrialize = serviceAgentAssembly.GetTypes().Where(x => x.Name == "JsonConvert").Select(y => y.GetRuntimeMethods().Where(z => z.Name == "DeserializeObject" && z.ReturnType.Name == "Object" && z.GetParameters().Length == 1)).FirstOrDefault();
            dny = method_desrialize.FirstOrDefault().Invoke(securityvalue, new object[] { configJson });
            dny["Cache"]["Server"] = PegAppPath.Redisserver;

            var method_serialize = serviceAgentAssembly.GetTypes().Where(x => x.Name == "JsonConvert").Select(y => y.GetRuntimeMethods().Where(z => z.Name == "SerializeObject" && z.ReturnType.Name == "String" && z.GetParameters().Length == 2).ToList()).FirstOrDefault();
            jsondata = method_serialize.Select(x => x).Where(y => y.GetParameters().Any(z => z.ParameterType.FullName == "Newtonsoft.Json.Formatting")).FirstOrDefault().Invoke(jsondata, new object[] { dny, 1 }).ToString();

            File.WriteAllText(appsettingsjsonpath, jsondata);
            Console.WriteLine("\n Cachesetting server Updated");
        }

        public static Dictionary<string, string> GetAllNextGenAppPath()
        {
            Dictionary<string, string> spAppPath = new Dictionary<string, string>();

            var PegNxGenAppPath = Enum.GetValues(typeof(PegNextGenAppPath)).Cast<PegNextGenAppPath>().ToList();
            string AppName = "";

            foreach (var DevBranchName in PegNxGenAppPath)
            {
                string branchname = DevBranchName.ToString();

                AppName = DevBranchName.ToString();
                if (DevBranchName.ToString().Contains(MappingHelper.ngAdmin))
                {
                    if (!string.IsNullOrEmpty(PegAppPath.Ngc))
                    {
                        branchname = PegAppPath.Ngc;
                    }
                    AppName = Enum.GetName(typeof(NextGenAppPath), NextGenAppPath.Admin);
                }
                if (DevBranchName.ToString().Contains(MappingHelper.ngContent))
                {
                    if (!string.IsNullOrEmpty(PegAppPath.Ngc))
                    {
                        branchname = PegAppPath.Ngc;
                    }
                    AppName = Enum.GetName(typeof(NextGenAppPath), NextGenAppPath.Content);
                }
                if (DevBranchName.ToString().Contains(MappingHelper.ngIntegration))
                {
                    if (!string.IsNullOrEmpty(PegAppPath.Ngc))
                    {
                        branchname = PegAppPath.Ngc;
                    }
                    AppName = Enum.GetName(typeof(NextGenAppPath), NextGenAppPath.Integration);
                }
                if (AppName.Contains(MappingHelper.VS))
                {
                    AppName = MappingHelper.ViewSubmission;
                }
                if (AppName.Contains(MappingHelper.todaysview))
                {
                    AppName = Enum.GetName(typeof(NextGenAppPath), NextGenAppPath.Notifications);
                }
                string RepoName = "";
                MappingHelper.AppcsProjectName.TryGetValue(AppName, out RepoName);
                string spAppPathname = PegAppPath.PegNextGenAppPath + MappingHelper.BackwardSlash + branchname + MappingHelper.services + RepoName;
                spAppPath.Add(AppName + MappingHelper.API, spAppPathname);
                if (DevBranchName.ToString().Contains(MappingHelper.ngAdmin))
                {
                    spAppPath.Add(MappingHelper.CoreServiceProvider, PegAppPath.PegNextGenAppPath + MappingHelper.BackwardSlash + branchname + MappingHelper.services + MappingHelper.AuthProvider);
                }
            }
            return spAppPath;
        }

        public static void RunNextGenBatchScripts()
        {
            try
            {
                Dictionary<string, string> ScriptPaths = PegasusNextGen.GetAllNextGenSPAppPath(PegAppPath.PegNextGenAppPath);
                foreach (var ScriptPath in ScriptPaths)
                {
                    if (Directory.Exists(ScriptPath.Value.ToString()))
                    {
                        string batchfilefilename = @ScriptPath.Value + MappingHelper.BackwardSlash + MappingHelper.BatchFile;
                        if (!File.Exists(batchfilefilename))
                        {
                            using (StreamWriter writer = File.CreateText(batchfilefilename))
                            {
                                // write ExecuteMultipleSQLFileWithoutCredentials script into the file
                                writer.WriteLine(@"@Echo Off

                                        FOR /f %%i IN ('DIR *.Sql /B') do call :RunScript %%i
 
                                        GOTO :END

                                        :RunScript

                                        Echo Executing %1

                                        sqlcmd /S " + PegAppPath.Server + " /d " + PegAppPath.Database + @" -E -i %1 

                                        Echo Completed %1

                                       :END");
                            }
                        }

                        Console.Write("\nDo You Want to Execute Scripts for " + ScriptPath.Key + " ? Y/N: ");
                        ConsoleKeyInfo abc = ConfigHelper.ValidateUserInput();
                        if (abc.Key == ConsoleKey.Y)
                        {
                            string batDir = string.Format(@ScriptPath.Value);
                            Process proc = new Process();
                            proc.StartInfo.WorkingDirectory = batDir;
                            proc.StartInfo.FileName = MappingHelper.BatchFile;
                            proc.StartInfo.CreateNoWindow = true;
                            proc.Start();
                            proc.WaitForExit();
                            Console.WriteLine("\nScripts executed for " + ScriptPath.Key);
                        }
                    }
                    else
                    {
                        Console.WriteLine("\nScript path does not exists: " + ScriptPath.Value.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("\nException occured: " + ex.Message);
            }
        }
    }
}
