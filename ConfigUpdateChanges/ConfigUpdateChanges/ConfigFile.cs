﻿using System.Runtime.Serialization;

namespace ConfigUpdateChanges
{
    public class ConfigFile
    {
        public string PegAppPath { get; set; }
        public string PegNextGenAppPath { get; set; }
        public string PegasusDoamin { get; set; }
        public string Server { get; set; }
        public string UID { get; set; }
        public string PWD { get; set; }
        public string Ngc { get; set; }
        public string MsBuildPath { get; set; }
        public string Database { get; set; }
        public string Redisserver { get; set; }
        public string PegasusNotificationsAppPath { get; set; }
        public string PegasusNotificationsApiPath { get; set; }
        public string NextGenAuthProviderApiPath { get; set; }
        public string EnableNextGenViewSubmission { get; set; }
        public string NextGenGatewayApiPath { get; set; }
        public string NextGenViewSubmissionAppPath { get; set; }
        public string PegasusNextGenReportAppPath { get; set; }
        public string PegasusNextGenReportApiPath { get; set; }
        public string PegasusNextGenGradeBookApiPath { get; set; }
        public string PegasusNextGenGradebookAppPath { get; set; }
        public string PegasusNextGenContentApiPath { get; set; }
        public string NextGenAdminApiPath { get; set; }
        public string NextGenIntegrationApiPath { get; set; }
        public string NotificationContentApiPath { get; set; }
    }

    public class PegasusConfig
    {
        public ConfigFile Pegasus { get; set; }
    }

    public class ServerName
    {
        public static string DbServerName { get; set; }
    }

    [DataContract]
    public class connectionstring
    {
        [DataMember]
        public PegasusConnectionStrings PegasusConnectionStrings { get; set; }
    }

    [DataContract]
    public class PegasusConnectionStrings
    {
        [DataMember]
        public string PegasusAdminConnection { get; set; }

        [DataMember]
        public string PegasusDBConnection1 { get; set; }

        [DataMember]
        public string MasterCourseDBConnString { get; set; }

        [DataMember]
        public string ReportingConnection { get; set; }
    }
}
