﻿using System;
using System.Xml;

namespace ConfigUpdateChanges
{
    public class FeatureFlag
    {
        public static void UpdateFeatureFlag(string xmlFilePath)
        {
            Console.Write("\nDo You Want to Update Pegasus Feature Flag? Y/N: ");
            ConsoleKeyInfo abc = ConfigHelper.ValidateUserInput();
            if (abc.Key == ConsoleKey.Y)
            {
                UpdateSingleFeatureFlag(xmlFilePath, "PiSessionAuthEnabled");
            }
            Console.Write("\nDo You Want to Update SMSPIBridge Configs? Y/N: ");
            ConsoleKeyInfo smspibridge = ConfigHelper.ValidateUserInput();
            if (smspibridge.Key == ConsoleKey.Y)
            {
                Console.Write("\nDo You Want to Update MMNDSMSBridgeEnabled Pegasus Feature Flag? Y/N: ");
                ConsoleKeyInfo ccngbridge = ConfigHelper.ValidateUserInput();
                if (ccngbridge.Key == ConsoleKey.Y)
                {
                    UpdateSingleFeatureFlag(xmlFilePath, "IsCCNGSMSBridgeEnabled");
                }

                Console.Write("\nDo You Want to Update DeepLinkSMSBridgeEnabled Pegasus Feature Flag? Y/N: ");
                ConsoleKeyInfo deeplink = ConfigHelper.ValidateUserInput();
                if (deeplink.Key == ConsoleKey.Y)
                {
                    UpdateSingleFeatureFlag(xmlFilePath, "IsDeepLinkSMSBridgeEnabled");
                }

                Console.Write("\nDo You Want to Update TpiSMSBridgeEnabled Pegasus Feature Flag? Y/N: ");
                ConsoleKeyInfo tpi = ConfigHelper.ValidateUserInput();
                if (tpi.Key == ConsoleKey.Y)
                {
                    UpdateSingleFeatureFlag(xmlFilePath, "IsTpiSMSBridgeEnabled");
                }
            }
        }

        private static void UpdateSingleFeatureFlag(string xmlFilePath, string featureName)
        {
            Console.Write($"\nPlease select Pegasus Feature Flag {featureName} to update? Press Y - yes , N - no : ");
            ConsoleKeyInfo flags = ConfigHelper.ValidateUserInput();
            string flagValue = flags.Key == ConsoleKey.Y ? "yes" : "no";
            UpdateFeatureFlagconfig(xmlFilePath, featureName, flagValue);
        }

        public static void UpdateFeatureFlagconfig(string xmlFilePath, string featureName, string newValue)
        {
            try
            {
                // Load the XML document
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(xmlFilePath);

                // Find the FeatureFlag element
                XmlNode featureFlagNode = xmlDoc.SelectSingleNode("/pegasus.featureflag/FeatureFlag");

                // Find the add element with the given name attribute
                XmlNode addNode = featureFlagNode.SelectSingleNode($"add[@name='{featureName}']");

                if (addNode != null)
                {
                    // Update the value attribute
                    XmlAttribute valueAttribute = addNode.Attributes["value"];
                    valueAttribute.Value = newValue;
                }
                else
                {
                    // If the add element with the given name attribute does not exist, create a new one
                    XmlElement newAddElement = xmlDoc.CreateElement("add");
                    XmlAttribute nameAttribute = xmlDoc.CreateAttribute("name");
                    nameAttribute.Value = featureName;
                    newAddElement.Attributes.Append(nameAttribute);

                    XmlAttribute valueAttribute = xmlDoc.CreateAttribute("value");
                    valueAttribute.Value = newValue;
                    newAddElement.Attributes.Append(valueAttribute);

                    featureFlagNode.AppendChild(newAddElement);
                }

                // Save the changes to the XML document
                xmlDoc.Save(xmlFilePath);

                Console.WriteLine("\nXML configuration updated successfully.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("\nError updating XML configuration: " + ex.Message);
            }
        }
    }
}
