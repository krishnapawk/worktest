﻿using System;
using System.IO;
using System.Xml;
using System.Net;
using System.Linq;
using Microsoft.Win32;
using System.Diagnostics;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;
using Microsoft.Web.Administration;
using static ConfigUpdateChanges.Enumeration;

namespace ConfigUpdateChanges
{
    public class ConfigChanges
    {
        bool change = false;
        string PegasusPath = "";
        ConfigFile PegAppPath = new ConfigFile();
        protected FolderBrowserDialog _openFileDialog = new FolderBrowserDialog();

        public void SetPegasusServer()
        {
            RegistryView registryView = Environment.Is64BitOperatingSystem ? RegistryView.Registry64 : RegistryView.Registry32;
            using (RegistryKey hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, registryView))
            {
                RegistryKey instanceKey = hklm.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server\Instance Names\SQL", false);
                if (instanceKey != null)
                {
                    foreach (var instanceName in instanceKey.GetValueNames())
                    {
                        Console.Write("\nDo You Want to Set Server name " + Environment.MachineName + @"\" + instanceName + " ? Y/N: ");
                        ConsoleKeyInfo abc = ConfigHelper.ValidateUserInput();
                        if (abc.Key == ConsoleKey.Y)
                        {
                            PegAppPath.Server = Environment.MachineName + @"\" + instanceName;
                            ServerName.DbServerName = PegAppPath.Server;
                        }
                    }
                }
            }
        }

        public void ChangeConfigfile()
        {
            try
            {
                ConfigFile PegPath = CheckExistingFilePath();
                Console.Write("\nDo You Want to update Pegasus connectionstrings " + " ? Y/N: ");
                ConsoleKeyInfo abc = ConfigHelper.ValidateUserInput();
                if (abc.Key == ConsoleKey.Y)
                {
                    ConfigHelper.UpdatePegasusconfigConnectionString(PegAppPath.PegAppPath + "\\Pegasus.Config\\pegasus.connectionStrings.config");
                }

                Console.Write("\nDo You Want to update Nextgen connectionstrings " + " ? Y/N: ");
                ConsoleKeyInfo nextgc = ConfigHelper.ValidateUserInput();
                if (nextgc.Key == ConsoleKey.Y)
                {
                    PegasusNextGen.UpdateNextgenConnectionString();
                }
                Console.Write("\nDo You Want to update Nextgen Appsettiings " + " ? Y/N: ");
                ConsoleKeyInfo nextg = ConfigHelper.ValidateUserInput();
                if (nextg.Key == ConsoleKey.Y)
                {
                    PegasusNextGen.UpdateNextgenAppsettings();
                }
                Console.Write("\nDo You Want to update Nextgen Cache Server " + " ? Y/N: ");
                ConsoleKeyInfo nextgcache = ConfigHelper.ValidateUserInput();
                if (nextgcache.Key == ConsoleKey.Y)
                {
                    PegasusNextGen.UpdateNextGenCacheServer();
                }
                //Console.Write("\nDo You Want to update Gradebook Appsettings " + " ? Y/N: ");
                //ConsoleKeyInfo nextgb = ConfigHelper.ValidateUserInput();
                //if (nextgb.Key == ConsoleKey.Y)
                //{
                //    PegasusNextGen.updateappsettingsjson();
                //}
                string EnvConfig = PegPath.PegAppPath + "\\Pegasus.Config\\pegasus.environment.config";
                string PegasusFeatureFlag = PegPath.PegAppPath + "\\Pegasus.Config\\Pegasus.Featureflag.config";
                string WebConfig = PegPath.PegAppPath + "\\Web\\web.config";
                string cacheconfig = PegPath.PegAppPath + "\\Web\\Cache.config";
                // Read in Xml-file 
                //XmlReaderSettings readerSettings = new XmlReaderSettings();
                //readerSettings.IgnoreComments = false;
                //XmlReader readerdoc = XmlReader.Create("D:/Config Files/KP/pegasus.environment.config", readerSettings);
                //XmlReader readerxml = XmlReader.Create(WebConfig, readerSettings);
                XmlDocument doc = new XmlDocument();
                XmlDocument xml = new XmlDocument();
                XmlDocument cachexml = new XmlDocument();

                doc.Load(EnvConfig);
                xml.Load(WebConfig);
                cachexml.Load(cacheconfig);

                FeatureFlag.UpdateFeatureFlag(PegasusFeatureFlag);

                ConfigHelper.UpdateCacheServer(cachexml, cacheconfig, "Pegasus");

                List<Int32> Webconfigkeyvalues = Enum.GetValues(typeof(Webconfigkey)).Cast<Webconfigkey>().ToList().Select(x => Convert.ToInt32(x)).ToList();

                foreach (var Webconfigkeyval in Webconfigkeyvalues)
                {
                    XmlNodeList nodes = xml.SelectNodes(MappingHelper.CachingNames[Convert.ToInt16((Webconfigkey)Webconfigkeyval)]);
                    if (nodes.Count > 0)
                    {
                        // nodes.Item(0).RemoveAll();
                        for (int i = nodes.Count - 1; i >= 0; i--)  //Remove the nodes using iteration.
                        {
                            nodes[i].ParentNode.RemoveChild(nodes[i]);
                        }
                    }
                }

                CachingMethodsUpdate(xml);
                Console.Write("\nDo You Want to update Login Environment - Course Space/Work Space/PII Login Page" + " ? Y/N: ");
                ConsoleKeyInfo lgn = ConfigHelper.ValidateUserInput();
                if (lgn.Key == ConsoleKey.Y)
                {
                    Dictionary<String, object> EnvironMentConfigData = GetEnvironmentConfigData();
                    XmlElement Root = doc.DocumentElement;
                    XmlNode appNode = Root["appSettings"];
                    foreach (XmlNode node in appNode.ChildNodes)
                    {
                        if (node.Attributes != null)
                        {
                            try
                            {
                                string key = node.Attributes.GetNamedItem("key").Value;
                                string value = node.Attributes.GetNamedItem("value").Value;
                                if (EnvironMentConfigData.ContainsKey(key) && value != EnvironMentConfigData[key].ToString())
                                {
                                    node.Attributes.GetNamedItem("value").Value = EnvironMentConfigData[key].ToString();
                                    change = true;
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("\nException occurred : " + ex.Message);
                            }
                        }
                    }
                }

                RunPegasusBatchScripts();
                PegasusNextGen.RunNextGenBatchScripts();
                PegasusNextGen.BuildApplication();
                try
                {
                    if (change)  //Update web.config only if changes are made to web.config file
                    {
                        //Saving the document......................................................
                        xml.Save(WebConfig);
                        Console.WriteLine("\nWeb Config updated");
                        doc.Save(EnvConfig);
                        Console.WriteLine("\nEnvironment Config updated");
                    }
                    ServerManager server = new ServerManager();
                    var site = server.Sites.FirstOrDefault(s => s.Name == "Default Web Site");
                    List<string> lst = new List<string>();
                    for (int i = 0; i < site.Applications.Count; i++)
                    {
                        lst.Add(site.Applications[i].ToString());
                    }
                    if (site != null)
                    {
                        //stopping the sites...
                        site.Stop();
                        if (site.State == ObjectState.Stopped)
                        {
                            //Starting the sites...
                            site.Start();
                        }
                        Console.WriteLine("\nIIS Running Now, Press any Key to Exit");
                        Console.ReadKey();
                        Environment.Exit(0);
                    }
                    else
                    {
                        throw new InvalidOperationException("Could not find website!");
                    }
                }
                catch (IOException ex)
                {
                    Console.WriteLine("\nException occurred : " + ex.Message.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("\nException occurred : " + e.Message.ToString());
            }
        }

        private void RunPegasusBatchScripts()
        {
            try
            {
                string[] ScriptPaths = new string[] { MappingHelper.PegSPInstaller, MappingHelper.Schema_Upgrade, MappingHelper.Dev_Pegasus517_Upgrade };
                Console.Write("\nDo You Want to Execute Scripts for Pegasus? Y/N: ");
                ConsoleKeyInfo abc = ConfigHelper.ValidateUserInput();
                if (abc.Key == ConsoleKey.Y)
                {
                    foreach (var scriptName in ScriptPaths)
                    {
                        string spAppPathname = PegAppPath.PegAppPath + MappingHelper.PegasusSPAppPath[Convert.ToInt16((PegasusSPPath.PegSPPath))];
                        string batDir = string.Format(@spAppPathname);
                        Process proc = new Process();
                        proc.StartInfo.WorkingDirectory = batDir;
                        proc.StartInfo.FileName = scriptName;
                        proc.StartInfo.CreateNoWindow = true;
                        proc.Start();
                        proc.WaitForExit();
                        Console.WriteLine("\nScripts executed " + scriptName);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("\nException occurred : " + ex.Message);
            }
        }

        private XmlDocument UpdateXMLFile(XmlDocument xml, int pressedkey)
        {
            change = true;
            string someval = MappingHelper.sessionState[Convert.ToInt16(pressedkey)];
            //To uncomment entire outerXML
            XmlNodeList commentedNodes = xml.SelectNodes("//comment()");
            var commentNode = (from comment in commentedNodes.Cast<XmlNode>()
                               where comment.Value.Contains(someval)
                               select comment).FirstOrDefault();
            if (commentNode != null)
            {
                XmlReader nodeReader = XmlReader.Create(new StringReader(commentNode.Value));
                XmlNode newNode = xml.ReadNode(nodeReader);
                commentNode.ParentNode.ReplaceChild(newNode, commentNode);
            }
            List<Int32> Values = Enum.GetValues(typeof(Caching)).Cast<Caching>().ToList().Select(x => Convert.ToInt32(x)).ToList();
            Values.RemoveAll(x => x == pressedkey);
            foreach (var item in Values)
            {
                //To comment the entire outerXML
                XmlNode elementToComment = xml.SelectSingleNode(MappingHelper.CachingNames[Convert.ToInt16((Caching)item)]);
                if (elementToComment != null)
                {
                    // Get the XML content of the target node
                    String commentContents = elementToComment.OuterXml;

                    // Create a new comment node
                    // Its contents are the XML content of target node
                    XmlComment Nodetocomment = xml.CreateComment(commentContents);

                    // Get a reference to the parent of the target node
                    XmlNode parentNode = elementToComment.ParentNode;

                    // Replace the target node with the comment
                    parentNode.ReplaceChild(Nodetocomment, elementToComment);
                }
            }

            if (someval == MappingHelper.sessionState[Convert.ToInt16(Caching.RedisCache)])
            {
                Dictionary<String, object> WebConfigDataforRedis = SetWebConfigData();
                XmlNodeList MySessionStateStorenodelist = xml.SelectNodes(MappingHelper.CachingNames[Convert.ToInt16(Con.ConnectionString)]);
                if (MySessionStateStorenodelist.Count > 0)
                {
                    foreach (XmlElement node in MySessionStateStorenodelist)
                    {
                        node.Attributes.GetNamedItem("connectionString").Value = WebConfigDataforRedis["MySessionStateStore"].ToString();
                    }
                }
            }
            return xml;
        }

        private void CachingMethodsUpdate(XmlDocument xml)
        {
            Console.Write("\nDo you want to update caching Methods => InProc/Redis/Asp.netCaching Y|N : ");
            ConsoleKeyInfo abc = ConfigHelper.ValidateUserInput();
            if (abc.Key == ConsoleKey.Y)
            {
                Console.Write("\nPlease select the caching Methods => InProc : 1 , Redis : 2 , Asp.netCaching : 3 ===> ");
                ConsoleKeyInfo consoleKey = PressedKey(Console.ReadKey(), xml);
                char key = (char)consoleKey.Key;
                if (char.IsDigit(key) || consoleKey.Key == ConsoleKey.NumPad1 || consoleKey.Key == ConsoleKey.NumPad2 || consoleKey.Key == ConsoleKey.NumPad3)
                {
                    int pressedkey = int.Parse(consoleKey.KeyChar.ToString());
                    Console.WriteLine("\n" + (Caching)pressedkey + " Selected");
                    UpdateXMLFile(xml, pressedkey);
                }
            }

        }

        private ConsoleKeyInfo PressedKey(ConsoleKeyInfo consoleKey, XmlDocument xml)
        {
            if (!(consoleKey.Key == ConsoleKey.D1 || consoleKey.Key == ConsoleKey.NumPad1 || consoleKey.Key == ConsoleKey.D2 || consoleKey.Key == ConsoleKey.NumPad2 || consoleKey.Key == ConsoleKey.D3 || consoleKey.Key == ConsoleKey.NumPad3))
            {
                Console.WriteLine("\n Please provide valid key");
                CachingMethodsUpdate(xml);
            }
            return consoleKey;
        }

        private ConfigFile CheckExistingFilePath()
        {
            try
            {
                List<string> AppName = new List<string>();
                Dictionary<string, string> ApplicationPath = new Dictionary<string, string>();
                string MYPATH = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Pegasus");
                if (!Directory.Exists(MYPATH))
                {
                    Directory.CreateDirectory(MYPATH);
                    FolderBrowserDialog b = new FolderBrowserDialog();
                    Console.Write("Please Select Pegasus App Root Folder Path : Press Y to Select : ");
                    ConsoleKeyInfo abc = ConfigHelper.ValidateUserInput();
                    if (abc.Key == ConsoleKey.Y)
                    {
                        if (b.ShowDialog() == DialogResult.OK)
                        {
                            PegAppPath.PegAppPath = b.SelectedPath;
                            ApplicationPath.Add("PegAppPath", PegAppPath.PegAppPath);
                        }
                    }
                    b.Dispose();
                    ApplicationPath.Add("PegNextGenAppPath", AddPegasusNextGenPath());

                    SetPegasusServer();

                    Dictionary<string, object> nestedjsoncontent = SetInitialConfigDetails(ApplicationPath);
                    //File.WriteAllLines(MYPATH + MappingHelper.PegasusEntryPath, ApplicationPath.Select(x => x.Key + "=" + x.Value).ToArray());
                    File.WriteAllText(MYPATH + MappingHelper.PegasusEntryPath, ConfigHelper.JsonSerialize(nestedjsoncontent, PegAppPath.PegAppPath));
                }
                else if (!File.Exists(MYPATH + MappingHelper.PegasusEntryPath))
                {
                    FolderBrowserDialog b = new FolderBrowserDialog();
                    Console.Write("Please Select Pegasus App Root Folder Path : Press Y to Select : ");
                    ConsoleKeyInfo abc = ConfigHelper.ValidateUserInput();
                    if (abc.Key == ConsoleKey.Y)
                    {
                        if (b.ShowDialog() == DialogResult.OK)
                        {
                            PegAppPath.PegAppPath = b.SelectedPath;
                            ApplicationPath.Add("PegAppPath", PegAppPath.PegAppPath);
                        }
                    }
                    b.Dispose();
                    ApplicationPath.Add("PegNextGenAppPath", AddPegasusNextGenPath());
                    SetPegasusServer();

                    Dictionary<string, object> nestedjsoncontent = SetInitialConfigDetails(ApplicationPath);

                    //File.WriteAllLines(MYPATH + MappingHelper.PegasusEntryPath, ApplicationPath.Select(x => x.Key + "=" + x.Value).ToArray());
                    File.WriteAllText(MYPATH + MappingHelper.PegasusEntryPath, ConfigHelper.JsonSerialize(nestedjsoncontent, PegAppPath.PegAppPath));

                    Console.WriteLine("\nNote : Please update your DB details to this path " + MYPATH + MappingHelper.PegasusEntryPath + " \n" +

@"----------------------------- " + " \n" +
"Server" + PegAppPath.Server + " \n" +
@"Database=Pegasus517
UID=sa
PWD=Sa123
ngc=ngcoreservices 
----------------------------");
                    Console.WriteLine("\nWe have updated above DB details by default. Please update the value to PegasusFolderEntryPath.txt, If required. Press any Key to Exit");
                    Console.ReadKey();
                    Environment.Exit(0);
                }
                else
                {
                    //SetPegasusServer();
                    PegAppPath = ConfigHelper.GetTextData(MYPATH);
                }
                return PegAppPath;
            }
            catch (Exception ex)
            {
                Console.WriteLine("\nException occurred : " + ex.Message);
                throw ex;
            }
        }

        private Dictionary<string, object> SetInitialConfigDetails(Dictionary<string, string> ApplicationPath)
        {
            ApplicationPath.Add("Server", PegAppPath.Server);
            ApplicationPath.Add("Database", "Pegasus517");
            ApplicationPath.Add("UID", "sa");
            ApplicationPath.Add("PWD", "Sa123456");
            ApplicationPath.Add("Ngc", "ngcoreservices");
            ApplicationPath.Add("MsBuildPath", "C:\\Program Files\\Microsoft Visual Studio\\2022\\Professional\\MSBuild\\Current\\Bin\\MSBuild.exe");
            ApplicationPath.Add("Redisserver", "127.0.0.1:6379, syncTimeout =5000,connectTimeout=30000");
            ApplicationPath.Add("PegasusDoamin", "http://localhost");
            ApplicationPath.Add("PegasusNotificationsAppPath", "http://localhost:4201");
            ApplicationPath.Add("PegasusNotificationsApiPath", "http://localhost/pegasusnotifications/");
            ApplicationPath.Add("NextGenAuthProviderApiPath", "http://localhost/AuthProvider/api/");
            ApplicationPath.Add("EnableNextGenViewSubmission", "1");
            ApplicationPath.Add("NextGenGatewayApiPath", "http://localhost/NextGenAPIGateway/");
            ApplicationPath.Add("NextGenViewSubmissionAppPath", "http://localhost:4203");
            ApplicationPath.Add("PegasusNextGenReportAppPath", "http://localhost:4202");
            ApplicationPath.Add("PegasusNextGenReportApiPath", "http://localhost/ReportsAPI/api/");
            ApplicationPath.Add("PegasusNextGenGradeBookApiPath", "http://localhost/GradeBook.API/api/public/v1/organization/");
            ApplicationPath.Add("PegasusNextGenGradebookAppPath", "http://localhost:4200");
            ApplicationPath.Add("PegasusNextGenContentApiPath", "http://localhost/Content/api/public/v1/organization/");
            ApplicationPath.Add("NextGenAdminApiPath", "http://localhost/Admin.API/api/public/v1/");
            ApplicationPath.Add("NextGenIntegrationApiPath", "http://localhost/Integration/api/v1/");
            ApplicationPath.Add("NotificationContentApiPath", "http://localhost/Content/api/");

            return Setnestedcontent(ApplicationPath);

        }

        private Dictionary<string, object> Setnestedcontent(Dictionary<string, string> ApplicationPath)
        {
            Dictionary<string, object> jsoncontent = new Dictionary<string, object>()
            {
                { "Pegasus", ApplicationPath },
                { "gradebook.API", new Dictionary<string, string>
                    {
                        { "PIIUserServiceEndPoint", "http://localhost/AuthProvider/api/" },
                        { "GradeBookURI", "http://localhost/GradeBook.API/api/"},
                        { "ContentServiceURI", "http://localhost/Content/api/"},
                        { "AdminServiceURI", "http://localhost/Admin/api/public/v1/" },
                        { "ViewSubmissionAPI", "http://localhost/ViewSubmission.API/api/"},
                        { "IntegrationServiceURI", "http://localhost/Integration/api/v1/"},
                        {"IsRabbitmqEnabled","false" },
                        {"IsAutobahnEnabled","false" }
                    }
                },
                { "Content.API", new Dictionary<string, string>
                    {
                        {"AdminServiceURI", "http://localhost/Admin/api/public/v1/" }
                    }
                },
                { "Admin.API", new Dictionary<string, string>
                    {
                        { "PiiUserServiceEndPoint", "http://localhost/AuthProvider/api/" }
                    }
                },
                { "CoreServiceProvider", new Dictionary<string, string>
                    {
                        { "AdminServiceUri", "http://localhost/Admin/api/public/v1/course/" }
                    }
                },
                { "Integration.API", new Dictionary<string, string>
                    {
                        {"ContentServiceURI", "http://localhost/Content/api/" },
                        {"AuthProviderURI", "http://localhost/AuthProvider/api/" }
                    }
                },
                { "reports.API", new Dictionary<string, string>
                    {
                        {"ReportURI", "http://localhost/ReportsAPI/api/public/v1/organizations/" },
                        {"ContentServiceURI", "http://localhost/Content/api/" },
                        {"AdminServiceURI", "http://localhost/Admin/api/public/v1/" }
                    }
                },
                { "ViewSubmission.API", new Dictionary<string, string>
                    {
                        {"GradeBookUrl", "http://localhost/GradeBook.API/api/public/v1/organization/" },
                        {"UserServiceEndPoint", "http://localhost/AuthProvider/api/" },
                        {"AdminServiceEndPoint", "http://localhost/Admin/api/public/v1/" },
                        {"ContentServiceUri", "http://localhost/Content/api/" }
                    }
                }
            };

            return jsoncontent;
        }

        private string AddPegasusNextGenPath()
        {
            try
            {
                Console.Write("\nPlease Select NextGen App Folder Path : Press Y to Select : ");
                ConsoleKeyInfo abc = ConfigHelper.ValidateUserInput();
                if (abc.Key == ConsoleKey.Y)
                {
                    if (_openFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        PegAppPath.PegNextGenAppPath = _openFileDialog.SelectedPath;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occurred : " + ex.Message);
            }
            return PegAppPath.PegNextGenAppPath;
        }

        private Dictionary<string, object> GetEnvironmentConfigData()
        {
            Console.Write("\nPlease Select Environment => Course Space :1 ,  Work Space : 2 , PII Login Page : 3 ==> ");
            ConsoleKeyInfo abc = Console.ReadKey();
            return SetEnvironMentConfigData(abc);
        }

        private Dictionary<string, object> SetWebConfigData()
        {
            Dictionary<String, object> inputData = new Dictionary<string, object>
            {
                //inputData.Add("MySessionStateStore", "192.168.3.63:6380,192.168.3.189:6380,192.168.6.129:6380,192.168.6.180:6380,192.168.3.149:6380,192.168.5.66:6380, syncTimeout =5000,connectTimeout=30000");
                { "MySessionStateStore", PegAppPath.Redisserver }
            };
            return inputData;
        }

        private Dictionary<string, object> SetEnvironMentConfigData(ConsoleKeyInfo EnvType)
        {
            Dictionary<String, object> inputData = new Dictionary<string, object>();
            if (EnvType.Key == ConsoleKey.D1 || EnvType.Key == ConsoleKey.NumPad1)
            {
                inputData.Add("#DefaultHostHeaderURL#", "local.pearson.com");
                inputData.Add("#WSDefaultHostHeaderURL#", "localws.pearson.com");
                inputData.Add("#InstanceName#", "0");
                inputData.Add("#IsInboundSSO#", "1");
                inputData.Add("#SMSAuthentication#", "Yes");
                inputData.Add("#SMSJITRegistration#", "Yes");
                inputData.Add("#PIAuthentication#", "No");
            }
            else if (EnvType.Key == ConsoleKey.D2 || EnvType.Key == ConsoleKey.NumPad2)
            {
                inputData.Add("#DefaultHostHeaderURL#", "local.pearson.com");
                inputData.Add("#WSDefaultHostHeaderURL#", "localws.pearson.com");
                inputData.Add("#InstanceName#", "0");
                inputData.Add("#IsInboundSSO#", "0");
                inputData.Add("#SMSAuthentication#", "No");
                inputData.Add("#SMSJITRegistration#", "No");
                inputData.Add("#PIAuthentication#", "No");
            }
            else if (EnvType.Key == ConsoleKey.D3 || EnvType.Key == ConsoleKey.NumPad3)
            {
                inputData.Add("#DefaultHostHeaderURL#", "local.pearson.com");
                inputData.Add("#WSDefaultHostHeaderURL#", "localws.pearson.com");
                inputData.Add("#InstanceName#", "0");
                inputData.Add("#IsInboundSSO#", "1");
                inputData.Add("#SMSAuthentication#", "No");
                inputData.Add("#SMSJITRegistration#", "No");
                inputData.Add("#PIAuthentication#", "Yes");
            }
            else
            {
                Console.WriteLine("\n Please provide valid Environment Config");
                GetEnvironmentConfigData();
            }
            inputData.Add("#PegasusNotificationsAppPath#", PegAppPath.PegasusNotificationsAppPath);
            //< !--Domain name of the Pegasus Notification API(REST) project-- >
            inputData.Add("#PegasusNotificationsApiPath#", PegAppPath.PegasusNotificationsApiPath);
            //< !--Domain name of the Next Gen Auth Provider Api Path API(REST) project-- >
            inputData.Add("#NextGenAuthProviderApiPath#", PegAppPath.NextGenAuthProviderApiPath);
            //< !--To enable the next gen View submission UI-- >
            inputData.Add("#EnableNextGenViewSubmission#", PegAppPath.EnableNextGenViewSubmission);
            //< !--Domain name of the Next Gen Gateway Api Path API(REST) project-- >
            inputData.Add("#NextGenGatewayApiPath#", PegAppPath.NextGenGatewayApiPath);
            //< !--Domain name of the NextGen ViewSubmission UI project-- >
            inputData.Add("#NextGenViewSubmissionAppPath#", PegAppPath.NextGenViewSubmissionAppPath);
            // < !--Domain name of the NextGen report UI project-- >
            inputData.Add("#PegasusNextGenReportAppPath#", PegAppPath.PegasusNextGenReportAppPath);
            //< !--Domain name of the Next Gen Reports Api Path API(REST) project-- >
            inputData.Add("#PegasusNextGenReportApiPath#", PegAppPath.PegasusNextGenReportApiPath);
            //< !--Domain name of the Next Gen GradeBook Api Path API(REST) project-- >
            inputData.Add("#PegasusNextGenGradeBookApiPath#", PegAppPath.PegasusNextGenGradeBookApiPath);
            inputData.Add("#PegasusNextGenGradebookAppPath#", PegAppPath.PegasusNextGenGradebookAppPath);
            //<!-- Domain name of the NextGen Content Core.API project -->
            inputData.Add("#PegasusNextGenContentApiPath#", PegAppPath.PegasusNextGenContentApiPath);
            //<!-- Domain name of the NextGen Admin Core.API project -->
            inputData.Add("#NextGenAdminApiPath#", PegAppPath.NextGenAdminApiPath);
            //<!-- Domain name of the NextGen Integration App project -->
            inputData.Add("#NextGenIntegrationApiPath#", PegAppPath.NextGenIntegrationApiPath);
            inputData.Add("#AssemblyPath#", PegasusPath + "\\web\\bin\\");
            return inputData;
        }

        //public async Task FileTransferProgressBars()
        //{

        //    List<string> urls = new List<string>();
        //    urls.Add("https://webpihandler.azurewebsites.net/web/handlers/webpi.ashx/getinstaller/urlrewrite2.appids");

        //    await TestFileTransferProgressBar(urls);

        //    Console.ReadLine();
        //}

        //private static void ProgressChanged(object sender, DownloadProgressChangedEventArgs e, Stopwatch sw, FileTransferProgressBar progress, int left, int top)
        //{

        //    progress.BytesReceived = e.BytesReceived;
        //    var speed = string.Format("{0} Kb/s ", (e.BytesReceived / 1024d / sw.Elapsed.TotalSeconds).ToString("0.00"));
        //    var percent = (double)e.BytesReceived / e.TotalBytesToReceive;
        //    progress.Report(percent, speed);


        //}
        //public static long GetFileSize(string url)
        //{
        //    long result = -1;

        //    System.Net.WebRequest req = System.Net.WebRequest.Create(url);
        //    req.Method = "HEAD";
        //    using (System.Net.WebResponse resp = req.GetResponse())
        //    {
        //        if (long.TryParse(resp.Headers.Get("Content-Length"), out long ContentLength))
        //        {
        //            result = ContentLength;
        //        }
        //    }

        //    return result;
        //}

        //public static void ClearCurrentConsoleLine(int currentLineCursor)
        //{

        //    Console.SetCursorPosition(0, currentLineCursor);
        //    Console.Write(new string(' ', Console.WindowWidth));
        //    Console.SetCursorPosition(0, currentLineCursor);
        //}
        //private static async Task TestFileTransferProgressBar(List<string> urls)
        //{
        //    var listTask = new List<Task>();
        //    await Task.Run(() =>
        //    {
        //        foreach (var item in urls)
        //        {
        //            Stopwatch sw = new Stopwatch();
        //            using (var webClient = new WebClient())
        //            {
        //                Uri URL = new Uri(item);
        //                var totalBytes = GetFileSize(item);
        //                var textCaption = $"File {Path.GetFileName(item)} transfer in progress... ";
        //                Console.Write(textCaption);
        //                int left = Console.CursorLeft;
        //                int top = Console.CursorTop;

        //                var progress = new FileTransferProgressBar(totalBytes, TimeSpan.FromSeconds(5))
        //                {
        //                    NumberOfBlocks = 20,
        //                    StartBracket = "|",
        //                    EndBracket = "|",
        //                    CompletedBlock = "|",
        //                    IncompleteBlock = "\u00a0",
        //                    AnimationSequence = ProgressAnimations.PulsingLine

        //                };


        //                webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler((sender, e) => ProgressChanged(sender, e, sw, progress, left, top)); ;

        //                sw.Start();
        //                try
        //                {

        //                    webClient.DownloadFileAsync(URL, Path.GetFileName(item));
        //                }
        //                catch (WebException ex1)
        //                {
        //                    Console.WriteLine(ex1.Message);
        //                }
        //                catch (Exception ex)
        //                {
        //                    Console.WriteLine(ex.Message);
        //                }

        //            }

        //        }
        //    });


        //}


        //private void client_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e) // This is our new method!
        //{
        //    Console.WriteLine("\nFile has been downloaded!");

        //    System.Diagnostics.Process.Start(Pegasustemp);
        //}
        //private void downloadfiles()
        //{
        //    Console.Write("\nDo You Want to download files " + " ? Y/N: ");
        //    ConsoleKeyInfo abc = Console.ReadKey();
        //    if (abc.Key == ConsoleKey.Y)
        //    {
        //        using (var client = new WebClient())
        //        {
        //            client.DownloadFileCompleted += client_DownloadFileCompleted; // Add our new event handler
        //            Console.WriteLine("\nFile will start downloading");

        //            client.DownloadFileAsync(new Uri("https://webpihandler.azurewebsites.net/web/handlers/webpi.ashx/getinstaller/urlrewrite2.appids"), Pegasustemp);
        //        }
        //    }

        //}

        public void downloadfiles()
        {
            var source = new Uri("https://webpihandler.azurewebsites.net/web/handlers/webpi.ashx/getinstaller/urlrewrite2.appids");
            string dest = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Pegasus/urlrewrite.exe");
            Console.Write("\nDo You Want to download files " + " ? Y/N: ");
            ConsoleKeyInfo abc = ConfigHelper.ValidateUserInput();
            if (abc.Key == ConsoleKey.Y)
            {
                var client = new WebClient();
                client.DownloadProgressChanged += OnDownloadProgressChanged;
                client.DownloadFileCompleted += OnDownloadComplete;
                client.DownloadFileAsync(source, dest, dest);
            }
        }

        private void OnDownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            // report progress
            Console.WriteLine(string.Format("{0} downloaded {1} of {2} bytes. {3}% complete", (string)e.UserState, e.BytesReceived, e.TotalBytesToReceive, e.ProgressPercentage));
        }

        private void OnDownloadComplete(object sender, AsyncCompletedEventArgs e)
        {
            // clean up
            var client = (WebClient)sender;
            client.DownloadProgressChanged -= OnDownloadProgressChanged;
            client.DownloadFileCompleted -= OnDownloadComplete;

            if (e.Error != null)
                throw e.Error;

            if (e.Cancelled)
                Environment.Exit(1);

            // install program
            var downloadedFile = (string)e.UserState;
            var processInfo = new ProcessStartInfo(downloadedFile, "/S");
            processInfo.CreateNoWindow = true;

            var installProcess = Process.Start(processInfo);
            installProcess.WaitForExit();

            Environment.Exit(installProcess.ExitCode);
        }
    }
}
